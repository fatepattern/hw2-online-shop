import { useEffect, useState } from 'react'
import './App.css'
import Button from './components/Button/Button'
import ModalImage from './components/Modal/ModalImage';
import ModalText from './components/Modal/ModalText';
import Header from './components/Header/Header';
import Products from './components/Products/Products';

function App() {


  const [products, setProducts] = useState();
  const [favouritesList, setFavouritesList] = useState();

  useEffect(() => {
    async function getProducts(){
      try{
        const response = await fetch('../public/products.json');
        if(!response.ok){
          throw new Error('Failed to fetch products')
        }
        const data = await response.json();
        setProducts(data);
      } catch (e){
        console.log(e.message);
      }
    }

    getProducts()
  }, [])

  const [cart, addToCart] = useState([]);


  return (
    <>

      <Header/>
      <Products cart={cart} addToCart={addToCart} products={products} favouritesList={favouritesList} setFavouritesList={setFavouritesList} />
      

      

    </>
  )
}

export default App