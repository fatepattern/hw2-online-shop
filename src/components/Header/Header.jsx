import React from 'react';
import "./Header.scss";

const Header = () => {

    const storedCartItems = localStorage.getItem('cartItems');
    let amountOfItems;

    if(storedCartItems){
        amountOfItems = JSON.parse(storedCartItems).length;
    } else {
        amountOfItems = 0;
    }

    let amountOfFavourites = 0;

    const storedFavourites = JSON.parse(localStorage.getItem('favourites'));

    if(storedFavourites){
        amountOfFavourites = storedFavourites.length;
    }


    return(
        <div className='header'>
            <p className='header__title'>Skins</p>

            <div className='header__icons'>

                <div className='header__icons--favourites-container'>
                    <svg className="header__icons--favourites" fill="#000000" height="800px" width="800px" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
                    viewBox="0 0 455 455" xml:space="preserve">
                    <path d="M326.632,10.346c-38.733,0-74.991,17.537-99.132,46.92c-24.141-29.384-60.398-46.92-99.132-46.92
                    C57.586,10.346,0,67.931,0,138.714c0,55.426,33.05,119.535,98.23,190.546c50.161,54.647,104.728,96.959,120.257,108.626l9.01,6.769
                    l9.01-6.768c15.529-11.667,70.098-53.978,120.26-108.625C421.949,258.251,455,194.141,455,138.714
                    C455,67.931,397.414,10.346,326.632,10.346z M334.666,308.974c-41.259,44.948-85.648,81.283-107.169,98.029
                    c-21.52-16.746-65.907-53.082-107.166-98.03C61.236,244.592,30,185.717,30,138.714c0-54.24,44.128-98.368,98.368-98.368
                    c35.694,0,68.652,19.454,86.013,50.771l13.119,23.666l13.119-23.666c17.36-31.316,50.318-50.771,86.013-50.771
                    c54.24,0,98.368,44.127,98.368,98.368C425,185.719,393.763,244.594,334.666,308.974z"/>
                    </svg>

                    <p className='header__icons--favourite--items'>{amountOfFavourites}</p>
                </div>

                <div className='header__icons--cart-container'>  
                    <svg className='header__icons--cart' fill="#000000" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" 
                        width="800px" height="800px" viewBox="0 0 902.86 902.86"
                        xml:space="preserve">
                    <g>
                        <g>
                            <path d="M671.504,577.829l110.485-432.609H902.86v-68H729.174L703.128,179.2L0,178.697l74.753,399.129h596.751V577.829z
                                M685.766,247.188l-67.077,262.64H131.199L81.928,246.756L685.766,247.188z"/>
                            <path d="M578.418,825.641c59.961,0,108.743-48.783,108.743-108.744s-48.782-108.742-108.743-108.742H168.717
                                c-59.961,0-108.744,48.781-108.744,108.742s48.782,108.744,108.744,108.744c59.962,0,108.743-48.783,108.743-108.744
                                c0-14.4-2.821-28.152-7.927-40.742h208.069c-5.107,12.59-7.928,26.342-7.928,40.742
                                C469.675,776.858,518.457,825.641,578.418,825.641z M209.46,716.897c0,22.467-18.277,40.744-40.743,40.744
                                c-22.466,0-40.744-18.277-40.744-40.744c0-22.465,18.277-40.742,40.744-40.742C191.183,676.155,209.46,694.432,209.46,716.897z
                                M619.162,716.897c0,22.467-18.277,40.744-40.743,40.744s-40.743-18.277-40.743-40.744c0-22.465,18.277-40.742,40.743-40.742
                                S619.162,694.432,619.162,716.897z"/>
                        </g>
                    </g>
                    </svg>

                    <p className='header__icons--cart--items'>{amountOfItems}</p>
                </div>


            </div>

            


        </div>
    )
}

export default Header;