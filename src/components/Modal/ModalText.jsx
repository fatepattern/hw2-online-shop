import React from 'react';
import {Modal, ModalBody, ModalClose, ModalFooter, ModalHeader, ModalWrapper} from "./Modal.jsx"

const ModalText = ({onClose, firstText, buttonOneTitle, description}) => {
    return (
        <ModalWrapper onClickOutside={onClose}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={onClose}/>
                </ModalHeader>
                <ModalBody>
                </ModalBody>
                <ModalFooter firstText={firstText} buttonOneTitle={buttonOneTitle}>
                    <p className='product-description'>{description}</p>
                </ModalFooter>
            </Modal>
        </ModalWrapper>
    )
}

export default ModalText;